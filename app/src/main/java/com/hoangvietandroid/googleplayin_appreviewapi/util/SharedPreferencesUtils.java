package com.hoangvietandroid.googleplayin_appreviewapi.util;

public class SharedPreferencesUtils {
    private static final String RATED_KEY = "rated_key";

    private SharedPreferencesUtils() {
        // No-op
    }

    public static boolean isRated() {
        return SharedPrefsFactory.getInstance().get(RATED_KEY, Boolean.class);
    }

    public static void setRated(boolean value) {
        SharedPrefsFactory.getInstance().put(RATED_KEY, value);
    }
}
