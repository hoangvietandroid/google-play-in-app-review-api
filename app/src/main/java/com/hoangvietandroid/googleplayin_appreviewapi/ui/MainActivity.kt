package com.hoangvietandroid.googleplayin_appreviewapi.ui

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.play.core.review.ReviewManager
import com.google.android.play.core.review.ReviewManagerFactory
import com.google.android.play.core.review.testing.FakeReviewManager
import com.hoangvietandroid.googleplayin_appreviewapi.R
import com.hoangvietandroid.googleplayin_appreviewapi.adapter.PersonAdapter
import com.hoangvietandroid.googleplayin_appreviewapi.database.DatabaseClient
import com.hoangvietandroid.googleplayin_appreviewapi.model.Person
import com.hoangvietandroid.googleplayin_appreviewapi.util.SharedPreferencesUtils
import kotlinx.android.synthetic.main.activity_main.*

/**
 * Create by HoangViet
 */
class MainActivity : AppCompatActivity() {
    private val REQUEST_CODE_REGISTER = 1
    private lateinit var mAdapter: PersonAdapter
    private var personList = mutableListOf<Person>()
    private lateinit var manager: FakeReviewManager
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        mAdapter = PersonAdapter()

        manager = FakeReviewManager(this)

        recyclerViewMain.adapter = mAdapter
        recyclerViewMain.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        addPerson()
        fab.setOnClickListener {
            val intent = Intent(this, RegisterActivity::class.java)
            startActivityForResult(intent, REQUEST_CODE_REGISTER)
        }
        mAdapter.setOnItemClickListener(object : PersonAdapter.OnClickItemListener {
            override fun onClickItem(position: Int) {
                //no-option
            }
        })
        setSupportActionBar(toolbar)
        supportActionBar?.apply {
            setDisplayHomeAsUpEnabled(false)
            setDisplayShowTitleEnabled(true)
        }
    }

    private fun addPerson() {
        Thread(Runnable {
            personList = DatabaseClient.getDatabase(this@MainActivity).getAllListPerson()
            runOnUiThread {
                mAdapter.addListPerson(personList)
            }
        }).start()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_CODE_REGISTER && resultCode == Activity.RESULT_OK) {
            addPerson()
            if (!SharedPreferencesUtils.isRated())
                reviewApp()
        }
    }

    private fun reviewApp() {
        val request = manager.requestReviewFlow()
        request.addOnCompleteListener { task ->
            if (task.isSuccessful) {
                val reviewInfo = task.result
                Log.d("TAG", "reviewApp: " + reviewInfo.describeContents())
                val flow = manager.launchReviewFlow(this, reviewInfo)
                flow.addOnCompleteListener {
                    if (it.isSuccessful) {
                        SharedPreferencesUtils.setRated(true)
                        Toast.makeText(this, "In-app review finished", Toast.LENGTH_SHORT).show()
                    } else {
                        Toast.makeText(this, "Rating Failed", Toast.LENGTH_SHORT).show()
                    }
                }.addOnFailureListener {
                    Toast.makeText(this, "Rating Failed", Toast.LENGTH_SHORT).show()
                }
            } else {
                Toast.makeText(this, "In-App Request Failed", Toast.LENGTH_SHORT).show()
            }
        }
    }
}
