package com.hoangvietandroid.googleplayin_appreviewapi.ui

import android.app.Activity
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import com.hoangvietandroid.googleplayin_appreviewapi.R
import com.hoangvietandroid.googleplayin_appreviewapi.database.DatabaseClient
import com.hoangvietandroid.googleplayin_appreviewapi.model.Person
import kotlinx.android.synthetic.main.activity_register.*

/**
 * Create by HoangViet
 */
class RegisterActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
        initToolbar()
        btnSaveRegister.setOnClickListener {
            val firstName = edtFirstName.text.toString()
            val lastName = edtLastName.text.toString()
            val phoneNumber = edtPhoneNumber.text.toString()
            val email = edtEmail.text.toString()
            Thread(Runnable {
                val person = Person(0, firstName, lastName, phoneNumber, email)
                DatabaseClient.getDatabase(this@RegisterActivity).insertPerson(person)
                runOnUiThread {
                    setResult(Activity.RESULT_OK)
                    finish()
                }
            }).start()
        }
    }

    private fun initToolbar() {
        setSupportActionBar(toolbarAdd)
        supportActionBar?.apply {
            setDisplayHomeAsUpEnabled(true)
            setDisplayShowTitleEnabled(false)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
            return true
        }
        return super.onOptionsItemSelected(item)
    }
}