package com.hoangvietandroid.googleplayin_appreviewapi

import android.app.Application
import com.hoangvietandroid.googleplayin_appreviewapi.util.SharedPrefsFactory

class App : Application() {
    override fun onCreate() {
        super.onCreate()
        SharedPrefsFactory.getInstance().initConfig(this)
    }
}