package com.hoangvietandroid.googleplayin_appreviewapi.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * Create by HoangViet
 */
@Entity(tableName = "person_table")
data class Person(
    @PrimaryKey(autoGenerate = true)
    var id: Int,
    @ColumnInfo(name = "first_name")
    var firstName: String,
    @ColumnInfo(name = "last_name")
    var lastName: String,
    @ColumnInfo(name = "phone_number")
    var phone: String,
    @ColumnInfo(name = "email")
    var email: String
)