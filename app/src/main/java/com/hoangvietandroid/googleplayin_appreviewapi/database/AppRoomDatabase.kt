package com.hoangvietandroid.googleplayin_appreviewapi.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.hoangvietandroid.googleplayin_appreviewapi.model.Person

/**
 * Create by HoangViet
 */
@Database(entities = [Person::class], version = 1)
abstract class AppRoomDatabase : RoomDatabase() {
    abstract fun getPersonDao(): PersonDao
}
