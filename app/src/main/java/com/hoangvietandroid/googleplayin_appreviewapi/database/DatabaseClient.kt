package com.hoangvietandroid.googleplayin_appreviewapi.database

import android.content.Context
import androidx.room.Room
import com.hoangvietandroid.googleplayin_appreviewapi.model.Person

/**
 * Create by HoangViet
 */
class DatabaseClient private constructor(context: Context) {
    private var mDatabase: AppRoomDatabase? = null

    init {
        mDatabase = Room.databaseBuilder(context, AppRoomDatabase::class.java, "Person_database").build()
    }

    companion object {
        private var INSTANCE: DatabaseClient? = null
        fun getDatabase(context: Context): DatabaseClient {
            if (INSTANCE == null) {
                INSTANCE = DatabaseClient(context)
            }
            return INSTANCE!!
        }
    }

    fun getAllListPerson(): MutableList<Person> {
        return mDatabase!!.getPersonDao().getAllListPerson()
    }

    fun insertPerson(person: Person) {
        mDatabase!!.getPersonDao().insertPerson(person)
    }

    fun deletePerson(person: Person) {
        mDatabase!!.getPersonDao().deletePerson(person)
    }

    fun updatePerson(person: Person) {
        mDatabase!!.getPersonDao().updatePerson(person)
    }
}
