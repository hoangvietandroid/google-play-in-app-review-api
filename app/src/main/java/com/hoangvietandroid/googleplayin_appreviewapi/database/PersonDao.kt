package com.hoangvietandroid.googleplayin_appreviewapi.database

import androidx.room.*
import com.hoangvietandroid.googleplayin_appreviewapi.model.Person

/**
 * Create by HoangViet
 */
@Dao
interface PersonDao {
    @Query("SELECT * FROM person_table")
    fun getAllListPerson(): MutableList<Person>

    @Insert
    fun insertPerson(person: Person)

    @Delete
    fun deletePerson(person: Person)

    @Update
    fun updatePerson(person: Person)
}
