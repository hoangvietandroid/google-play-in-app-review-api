package com.hoangvietandroid.googleplayin_appreviewapi.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.hoangvietandroid.googleplayin_appreviewapi.R
import com.hoangvietandroid.googleplayin_appreviewapi.model.Person
import kotlinx.android.synthetic.main.item_person.view.*

/**
 * Create by HoangViet
 */
class PersonAdapter : RecyclerView.Adapter<PersonAdapter.PersonViewHolder>() {
    private var mPersonList = mutableListOf<Person>()
    private var mOnClickItemListener: OnClickItemListener? = null

    class PersonViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PersonViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_person, parent, false)
        return PersonViewHolder(view)
    }

    override fun getItemCount(): Int {
        return mPersonList.size
    }

    fun addListPerson(personList: MutableList<Person>) {
        this.mPersonList = personList
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: PersonViewHolder, position: Int) {
        val person = mPersonList[position]
        holder.itemView.tvNameItem.text = (person.firstName + " " + person.lastName)
        holder.itemView.tvPhoneItem.text = person.phone
        holder.itemView.setOnClickListener {
            mOnClickItemListener?.onClickItem(position)
        }

    }

    fun setOnItemClickListener(onClickItemListener: OnClickItemListener) {
        this.mOnClickItemListener = onClickItemListener
    }

    interface OnClickItemListener {
        fun onClickItem(position: Int)
    }
}